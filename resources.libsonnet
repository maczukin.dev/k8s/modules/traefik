{
  ingressRoute: (import './resources/ingress-route.libsonnet'),
  ingressRouteTCP: (import './resources/ingress-route-tcp.libsonnet'),
  ingressRouteUDP: (import './resources/ingress-route-udp.libsonnet'),
  middleware: (import './resources/middleware.libsonnet'),
}
