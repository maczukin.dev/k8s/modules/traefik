local k = import 'github.com/grafana/jsonnet-libs/ksonnet-util/kausal.libsonnet';

local service = k.core.v1.service;
local servicePort = k.core.v1.servicePort;

local defaults = {
  name: error 'must provide name',
  namespace: error 'must provide namespace',
  traefikPort: error 'must provide traefikPort',
  entrypoints: error 'must provide entrypoints',
};

function(params={})
  local config = defaults + params;
  assert std.isArray(config.entrypoints);

  {
    'service-internal':
      local name = '%s-internal' % config.name;
      service.new(
        name=name,
        selector=config.labels,
        ports=[
          servicePort.newNamed('traefik', config.traefikPort, config.traefikPort),
        ]
      ) +
      service.metadata.withLabels(
        config.labels {
          'app.kubernetes.io/instance': name,
        }
      ),
  } +
  {
    ['service-%s' % entrypoint.serviceName(config)]: entrypoint.service(config)

    for entrypoint in config.entrypoints
  }
