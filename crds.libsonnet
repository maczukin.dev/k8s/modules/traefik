{
  ingressRoute: import 'crds/ingressroute.json',
  ingressRouteTCP: import 'crds/ingressroutetcp.json',
  ingressRouteUDP: import 'crds/ingressrouteudp.json',
  middleware: import 'crds/middleware.json',
  middlewareTCP: import 'crds/middlewaretcp.json',
  serversTransport: import 'crds/serverstransport.json',
  TLSOption: import 'crds/tlsoption.json',
  TLSStore: import 'crds/tlsstore.json',
  traefikService: import 'crds/traefikservice.json',
}
