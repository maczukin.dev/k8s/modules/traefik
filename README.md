# K8S Traefik module in JSONNET

JSONNET based module for preparing [Traefik](https://traefik.io) deployment
on Kubernetes.

## License

MIT

## Author

Tomasz maczukin, 2021

