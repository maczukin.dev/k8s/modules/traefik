LB_LOCAL_HOST ?= 127.0.0.10

K3D_CLUSTER_NAME := traefik-test
KUBECONFIG := $(HOME)/.kube/config

.PHONY: k3d-cluster-create
k3d-cluster-create: SERVERS_COUNT ?= 1
k3d-cluster-create: AGENTS_COUNT ?= 5
k3d-cluster-create:
	@k3d cluster create $(K3D_CLUSTER_NAME) \
		--servers $(SERVERS_COUNT) \
		--agents $(AGENTS_COUNT) \
		--api-port $(LB_LOCAL_HOST):6443 \
		--k3s-server-arg --no-deploy=traefik \
		--port $(LB_LOCAL_HOST):80:80@loadbalancer \
		--port $(LB_LOCAL_HOST):443:443@loadbalancer \
		--subnet=auto
	@kubectl cluster-info

.PHONY: k3d-cluster-delete
k3d-cluster-delete:
	@k3d cluster delete $(K3D_CLUSTER_NAME)

.PHONY: k3d-cluster-recreate
k3d-cluster-recreate: k3d-cluster-delete k3d-cluster-create

.PHONY: k3d-cluster-start
k3d-cluster-start:
	@k3d cluster start $(K3D_CLUSTER_NAME)
	@kubectl cluster-info

.PHONY: k3d-cluster-stop
k3d-cluster-stop:
	@k3d cluster stop $(K3D_CLUSTER_NAME)

.PHONY: k3d-cluster-restart
k3d-cluster-restart: k3d-cluster-stop k3d-cluster-start

doTokenSecretFile := tanka/dev/digitalocean-token.secret
DIGITALOCEAN_TOKEN ?= unknown
ifneq (,$(wildcard $(doTokenSecretFile)))
DIGITALOCEAN_TOKEN := $(shell cat $(doTokenSecretFile))
endif

tkCommonFlags := --ext-str 'digitalocean-token=$(DIGITALOCEAN_TOKEN)'

TANKA_ENV ?= dev

.PHONY: manifests-eval
manifests-eval:
	@tk eval tanka/$(TANKA_ENV) $(tkCommonFlags)

.PHONY: manifests-show
manifests-show:
	@tk show tanka/$(TANKA_ENV) $(tkCommonFlags)

.PHONY: manifests-export
manifests-export:
	@rm -rf build/
	@tk export build/ tanka/$(TANKA_ENV) $(tkCommonFlags)

.PHONY: manifests-apply
manifests-apply:
	@tk apply tanka/$(TANKA_ENV) $(tkCommonFlags)
	@tk prune tanka/$(TANKA_ENV) $(tkCommonFlags)

.PHONY: manifests-delete
manifests-delete:
	@tk delete tanka/$(TANKA_ENV) $(tkCommonFlags)

CRDS := ingressroute \
		ingressroutetcp \
		ingressrouteudp \
		middleware \
		middlewaretcp \
		serverstransport \
		tlsoption \
		tlsstore \
		traefikservice
TRAEFIK_VERSION ?= v2.5.1

yaml2json := .tmp/bin/yaml2json

.PHONY: download-crd-definitions
download-crd-definitions: $(yaml2json)
	@for crd in $(CRDS); do \
		echo "https://github.com/traefik/traefik/raw/$(TRAEFIK_VERSION)/docs/content/reference/dynamic-configuration/traefik.containo.us_$${crd}s.yaml"; \
		curl -sL "https://github.com/traefik/traefik/raw/$(TRAEFIK_VERSION)/docs/content/reference/dynamic-configuration/traefik.containo.us_$${crd}s.yaml" | $(yaml2json) > "crds/$${crd}.json"; \
		done

$(yaml2json): GOPATH := $(PWD)/.tmp/
$(yaml2json):
	mkdir -p $(GOPATH)
	go get -u github.com/mbrukman/yaml2json/cmd/yaml2json
