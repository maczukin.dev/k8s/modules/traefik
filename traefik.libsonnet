local crds = import './crds.libsonnet';
local dashboard = import './dashboard.libsonnet';
local deployment = import './deployment.libsonnet';
local entrypoint = import './entrypoint.libsonnet';
local rbac = import './rbac.libsonnet';
local utils = import './resources.libsonnet';
local serviceMonitor = import './service-monitor.libsonnet';
local services = import './services.libsonnet';

local defaults = {
  name: 'traefik',
  namespace: 'default',
  labels: {
    'app.kubernetes.io/name': 'traefik',
    'app.kubernetes.io/component': 'loadbalancer',
  },
  serviceAccountName: 'traefik-ingress-controller',
  image: {
    name: 'traefik',
    version: 'v2.5.1',
  },
  traefikPort: 8080,
  entrypoints: [
    entrypoint.new('web', 80) +
    entrypoint.withPort(8000),
  ],
  dashboard+: {
    entrypoints: {
      insecure: 'web',
      secure: 'websecure',
    },
  },
  acme+: {
    httpChallenge+: {
      entrypoint: 'web',
    },
  },
  serviceMonitor: true,
};

function(params={})
  local _defaults =
    if std.objectHas(params, 'acme') && std.objectHas(params.acme, 'enabled') && params.acme.enabled then
      defaults
      {
        entrypoints+: [
          entrypoint.new('websecure', 443) +
          entrypoint.withPort(8443) +
          entrypoint.withCertResolver('acme'),
        ],
      }
    else
      defaults;

  local config = _defaults + params;
  assert std.isObject(config);

  {
    crd: crds,
    rbac: rbac(config),
    services: services(config),
    deployment: deployment(config),
    dashboard: dashboard(config),
  } +
  if config.serviceMonitor then
    {
      serviceMonitor: serviceMonitor(config),
    }
  else
    {}
