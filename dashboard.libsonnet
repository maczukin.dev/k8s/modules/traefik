local resources = import './resources.libsonnet';
local middleware = resources.ingressRoute.route.middleware;

local k = import 'github.com/grafana/jsonnet-libs/ksonnet-util/kausal.libsonnet';

local authMiddleware = function(auth, type, namespace)
  if std.objectHas(auth, type) then
    middleware.new('traefik-auth-%s' % type) +
    middleware.withNamespace(namespace);

local redirectMiddleware = function(namespace)
  middleware.new('http-to-https-redirect') +
  middleware.withNamespace(namespace);

local middlewares = {
  secure: {
    dashboard(params, namespace): authMiddleware(params.auth, 'dashboard', namespace),
    prometheus(params, namespace): authMiddleware(params.auth, 'prometheus', namespace),
    ping(params, namespace): authMiddleware(params.auth, 'ping', namespace),
  },
  insecure: {
    dashboard(params, namespace): redirectMiddleware(namespace),
    prometheus(params, namespace): redirectMiddleware(namespace),
    ping(params, namespace): redirectMiddleware(namespace),
  },
};

local dashboardIngressRoute = function(params, namespace, labels, type)
  local ir = resources.ingressRoute;
  local route = ir.route;
  local service = route.service;

  ir.new('dashboard-%s' % type) +
  ir.metadata.withNamespace(namespace) +
  ir.metadata.withLabels(labels) +
  ir.spec.withEntryPoints([params.entrypoints[type]]) +
  ir.spec.withRoutes([
    // dashboard
    route.new('Host(`%s`)' % [params.hostname]) +
    route.withServicesMixin([
      service.new('api@internal') + service.withKind('TraefikService'),
    ]) +
    route.withMiddlewares(std.prune([
      middlewares[type].dashboard(params, namespace),
    ])),
    // Prometheus metrics
    route.new('Host(`%s`)&&PathPrefix(`/metrics`)' % [params.hostname]) +
    route.withServicesMixin([
      service.new('prometheus@internal') + service.withKind('TraefikService'),
    ]) +
    route.withMiddlewares(std.prune([
      middlewares[type].prometheus(params, namespace),
    ])),
    // ping
    route.new('Host(`%s`)&&PathPrefix(`/ping`)' % [params.hostname]) +
    route.withServicesMixin([
      service.new('ping@internal') + service.withKind('TraefikService'),
    ]) +
    route.withMiddlewares(std.prune([
      middlewares[type].ping(params, namespace),
    ])),
  ])
;


local dashboardMiddlewares = function(params, namespace, labels)
  local m = resources.middleware;
  local basicAuthMiddleware = function(type)
    if std.objectHas(params.auth, type) then
      local authName = 'traefik-auth-%s' % type;
      m.new(authName) +
      m.metadata.withNamespace(namespace) +
      m.metadata.withLabels(labels) +
      m.withSpec({
        basicAuth: {
          secret: authName,
          namespace: namespace,
        },
      });

  std.prune([
    basicAuthMiddleware('dashboard'),
    basicAuthMiddleware('prometheus'),
    basicAuthMiddleware('ping'),
  ])
;

local dashboardSecrets = function(params, namespace, labels)
  local secret = k.core.v1.secret;
  local basicAuthSecret = function(type)
    if std.objectHas(params.auth, type) then
      local authName = 'traefik-auth-%s' % type;
      secret.new(
        authName,
        {
          users: std.base64(params.auth[type]),
        },
      ) +
      secret.metadata.withLabels(labels);

  std.prune([
    basicAuthSecret('dashboard'),
    basicAuthSecret('prometheus'),
    basicAuthSecret('ping'),
  ])
;

local defaults = {
  namespace: error 'must provide namespace',
  dashboard: {
    expose: false,
    hostname: error 'must provide hostname',
    entrypoints: error 'must provide entrypoints',
    auth: {},
  },
};

function(params)
  local config = defaults + params;
  local dashboard = config.dashboard;

  if dashboard.expose then
    {
      ingressRoutes: [
        dashboardIngressRoute(dashboard, config.namespace, config.labels, 'secure'),
        dashboardIngressRoute(dashboard, config.namespace, config.labels, 'insecure'),
      ],
      middlewares: dashboardMiddlewares(dashboard, config.namespace, config.labels),
      secrets: dashboardSecrets(dashboard, config.namespace, config.labels),
    }
  else
    {}
