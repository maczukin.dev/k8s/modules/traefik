local k = import 'github.com/grafana/jsonnet-libs/ksonnet-util/kausal.libsonnet';

local clusterRoleBinding = k.rbac.v1.clusterRoleBinding;

local defaults = {
  namespace: error 'must provide namespace',
  serviceAccountName: error 'must provide serviceAccountName',
};

function(params={}) {
  local p = self,
  _config:: defaults + params,

  clusterRoleBinding:
    clusterRoleBinding.new(p._config.serviceAccountName) +
    clusterRoleBinding.metadata.withLabels(p._config.labels) +
    clusterRoleBinding.roleRef.withName(p._config.serviceAccountName) +
    clusterRoleBinding.roleRef.withApiGroup('rbac.authorization.k8s.io') +
    clusterRoleBinding.roleRef.withKind('ClusterRole') +
    clusterRoleBinding.withSubjects([
      {
        kind: 'ServiceAccount',
        name: p._config.serviceAccountName,
        namespace: p._config.namespace,
      },
    ]),
}
