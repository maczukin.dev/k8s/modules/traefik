local tr = import './_traefik_resource.libsonnet';

{
  new(name): tr.new('IngressRouteUDP', name),
  metadata: tr.metadata,
  spec: {
    withEntryPoints(entrypoints): { spec+: { entryPoints: if std.isArray(v=entrypoints) then entrypoints else [entrypoints] } },
    withEntryPointsMixin(entrypoints): { spec+: { entryPoints+: if std.isArray(v=entrypoints) then entrypoints else [entrypoints] } },
    withRoutes(routes): { spec+: { routes: if std.isArray(v=routes) then routes else [routes] } },
    withRoutesMixin(routes): { spec+: { routes+: if std.isArray(v=routes) then routes else [routes] } },
  },
  route: {
    withServices(services): { services: if std.isArray(v=services) then services else [services] },
    withServicesMixin(services): { services+: if std.isArray(v=services) then services else [services] },
    service: {
      new(name): {
        name: name,
      },
      withPort(port): { port: port },
      withWeight(weight): { weight: weight },
    },
  },
}
