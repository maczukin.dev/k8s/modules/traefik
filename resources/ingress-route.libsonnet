local tr = import './_traefik_resource.libsonnet';

{
  new(name): tr.new('IngressRoute', name),
  metadata: tr.metadata,
  spec: {
    withEntryPoints(entrypoints): { spec+: { entryPoints: if std.isArray(v=entrypoints) then entrypoints else [entrypoints] } },
    withEntryPointsMixin(entrypoints): { spec+: { entryPoints+: if std.isArray(v=entrypoints) then entrypoints else [entrypoints] } },
    withRoutes(routes): { spec+: { routes: if std.isArray(v=routes) then routes else [routes] } },
    withRoutesMixin(routes): { spec+: { routes+: if std.isArray(v=routes) then routes else [routes] } },
  },
  route: {
    new(match): {
      kind: 'Rule',
      match: match,
    },
    withServices(services): { services: if std.isArray(v=services) then services else [services] },
    withServicesMixin(services): { services+: if std.isArray(v=services) then services else [services] },
    withMiddlewares(middlewares): { middlewares: if std.isArray(v=middlewares) then middlewares else [middlewares] },
    withMiddlewaresMixin(middlewares): { middlewares+: if std.isArray(v=middlewares) then middlewares else [middlewares] },
    service: {
      new(name): {
        name: name,
      },
      withPort(port): { port: port },
      withKind(kind): { kind: kind },
    },
    middleware: {
      new(name): {
        name: name,
      },
      withNamespace(namespace): { namespace: namespace },
    },
  },
}
