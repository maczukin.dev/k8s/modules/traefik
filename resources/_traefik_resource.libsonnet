{
  new(kind, name): {
    apiVersion: 'traefik.containo.us/v1alpha1',
    kind: kind,
    metadata: {
      name: name,
    },
  },
  metadata: {
    withNamespace(namespace): { metadata+: { namespace: namespace } },
    withLabels(labels): { metadata+: { labels: labels } },
    withLabelsMixin(labels): { metadata+: { labels+: labels } },
  },
}
