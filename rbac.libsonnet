local clusterRoleBinding = import './rbac/cluster-role-binding.libsonnet';
local clusterRole = import './rbac/cluster-role.libsonnet';
local serviceAccount = import './rbac/service-account.libsonnet';

function(params={})
  clusterRole(params) +
  clusterRoleBinding(params) +
  serviceAccount(params)
