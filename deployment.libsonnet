local k = import 'github.com/grafana/jsonnet-libs/ksonnet-util/kausal.libsonnet';

local deployment = k.apps.v1.deployment;
local deploymentSpec = deployment.spec;
local deploymentTemplate = deployment.spec.template;
local container = k.core.v1.container;
local containerPort = k.core.v1.containerPort;
local livenessProbeHTTPGet = container.livenessProbe.httpGet;
local volume = k.core.v1.volume;
local volumeMount = k.core.v1.volumeMount;
local pvc = k.core.v1.persistentVolumeClaim;

local acme = function(params)
  local tlsChallengeParams =
    if params.tlsChallenge.enabled then
      [
        '--certificatesresolvers.acme.acme.tlschallenge',
      ]
    else
      []
  ;
  local httpChallengeParams =
    if params.httpChallenge.enabled then
      [
        '--certificatesresolvers.acme.acme.httpchallenge',
        '--certificatesresolvers.acme.acme.httpchallenge.entrypoint=%s' % params.httpChallenge.entrypoint,
      ]
    else
      []
  ;
  local dnsChallengeParams =
    if params.dnsChallenge.enabled then
      [
        '--certificatesresolvers.acme.acme.dnschallenge',
        '--certificatesresolvers.acme.acme.dnschallenge.provider=%s' % params.dnsChallenge.provider,
        '--certificatesresolvers.acme.acme.dnschallenge.delaybeforecheck=%d' % params.dnsChallenge.delaybeforecheck,
        '--certificatesresolvers.acme.acme.dnschallenge.resolvers=%s' % std.join(',', params.dnsChallenge.resolvers),
      ]
    else
      []
  ;
  if params.enabled then
    [
      '--certificatesresolvers.acme',
      '--certificatesresolvers.acme.acme.email=%s' % params.email,
      '--certificatesresolvers.acme.acme.storage=%s' % std.join('/', [params.storage.path, params.storage.file]),
      '--certificatesresolvers.acme.acme.caserver=%s' % params.caserver,
    ] +
    tlsChallengeParams +
    httpChallengeParams +
    dnsChallengeParams
  else
    []
;

local defaults = {
  name: error 'must provide name',
  namespace: error 'must provide namespace',
  serviceAccountName: error 'must provide serviceAccountName',
  logLevel: 'WARN',
  deployment: {
    replicas: 1,
    minReadySeconds: 10,
  },
  image: {
    name: error 'must provide image.name',
    version: error 'must provide image.version',
  },
  traefikPort: error 'must provide traefikPort',
  entrypoints: error 'must provide entrypoints',
  dashboard: {
    insecure: false,
  },
  acme: {
    enabled: false,
    email: error 'must provide acme.email',
    caserver: 'https://acme-v02.api.letsencrypt.org/directory',
    storage: {
      path: '/acme',
      file: 'acme.json',
      volumeName: 'acme-storage',
      pvc: {
        name: error 'must provide acme.storage.pvc.name',
        storageClassName: error 'must provide acme.storage.pvc.storageClassName',
        request: {
          storage: '250m',
        },
      },
    },
    tlsChallenge: {
      enabled: false,
    },
    httpChallenge: {
      enabled: false,
      entrypoint: error 'must provide acme.httpChallenge.entrypoint',
    },
    dnsChallenge: {
      enabled: false,
      provider: error 'must provide acme.dnsChallenge.provider',
      delaybeforecheck: 30,
      resolvers: error 'must provide acme.dnsChallenge.resolvers',
    },
  },
  envs: [],
  volumes: [],
};

function(params={}) {
  local p = self,
  _config:: defaults + params,

  _args_default:: std.prune(
    [
      '--ping',
      '--api.dashboard',
      '--log',
      '--log.format=json',
      '--log.level=%s' % p._config.logLevel,
      '--accesslog',
      '--accesslog.format=json',
      '--metrics',
      '--metrics.prometheus',
      '--metrics.prometheus.buckets=0.1,0.2,0.35,0.75,1.5,2.75,5.0,10.0,15.0,30.0,60.0',
      '--metrics.prometheus.addEntryPointsLabels',
      '--metrics.prometheus.addServicesLabels',
      '--providers.kubernetescrd',
      '--providers.kubernetescrd.allowcrossnamespace',
      '--serverstransport.insecureskipverify',
    ] +
    if p._config.dashboard.insecure then
      ['--api.insecure']
    else
      ['--metrics.prometheus.manualrouting']
  ),
  _args_entrypoint::
    std.prune(std.flattenArrays([
      entrypoint.containerArgs()
      for entrypoint in p._config.entrypoints
    ])),
  _args_certresolvers:: std.prune(acme(p._config.acme)),

  _ports::
    [
      containerPort.newNamed(p._config.traefikPort, 'traefik'),
    ] +
    [
      entrypoint.containerPort()
      for entrypoint in p._config.entrypoints
    ],

  _containers:: [
    container.new(p._config.name, '%s:%s' % [p._config.image.name, p._config.image.version]) +
    livenessProbeHTTPGet.withPort(p._config.traefikPort) +
    livenessProbeHTTPGet.withPath('/ping') +
    container.withArgs(
      p._args_default +
      p._args_entrypoint +
      p._args_certresolvers
    ) +
    container.withEnv(p._config.envs) +
    container.withPorts(p._ports) +
    container.withVolumeMounts(
      std.prune([
        if p._config.acme.enabled then
          local s = p._config.acme.storage;
          volumeMount.withName(s.volumeName) +
          volumeMount.withMountPath(s.path),
      ]) +
      [
        volume.mountSpec
        for volume in p._config.volumes
      ]
    ),
  ],

  _volumes::
    std.prune([
      if p._config.acme.enabled then
        local s = p._config.acme.storage;
        volume.persistentVolumeClaim.withClaimName(s.pvc.name) +
        volume.withName(s.volumeName),
    ]) +
    [
      volume.spec
      for volume in p._config.volumes
    ],

  deployment:
    deployment.new(
      name=p._config.name,
      replicas=p._config.deployment.replicas,
      podLabels=p._config.labels,
      containers=p._containers,
    ) +
    deployment.metadata.withLabels(p._config.labels) +
    deploymentSpec.withMinReadySeconds(p._config.deployment.minReadySeconds) +
    deploymentTemplate.spec.withServiceAccountName(p._config.serviceAccountName) +
    deploymentTemplate.spec.withVolumes(p._volumes),

  acmeStoragePVC:
    if p._config.acme.enabled then
      local _pvc = p._config.acme.storage.pvc;
      pvc.new(_pvc.name) +
      pvc.metadata.withNamespace(p._config.namespace) +
      pvc.spec.withAccessModes(['ReadWriteOnce']) +
      pvc.spec.withStorageClassName(_pvc.storageClassName) +
      pvc.spec.resources.withRequests(_pvc.request)
    else
      {},
}
